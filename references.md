# SPN
Rede SP ou Rede de substituição-permutação

É uma série de operações matemáticas ligadas usadas em algoritmos de cifração
de bloco.

É um caso especial de iteração de cifras composta por cifras de substituição e
transposição



# Rede Feistel

Uma rede de Feistel é uma cifra iterada com uma função interna chamada função
rodada.[1]

Temos a função de rodada (F)
K0, K1, ...Kn, as subchaves para as rodadas 0,1,...n


Divide a entrada em 2 partes (L0 R0)

então temos que 
L(i+1) = Ri
R(i+1) = Li XOR F(Ri, Ki)


# SBox

Provê uma substituição de bits
É utilizado para obscurecer a relação entre a chave e o texto sifrado
(confusão)

# Related-key atack

Eli Biham.  New types of cryptanalytic attacks using related keys.J. Cryptology 7(4):229{246, 1994.

é uma classe de ataques cryptoanalíticos em que o atacate sabe ou escolhe a
relação entre diferentes chaves, e tem o acesso a funções de
encriptação/decriptação com todas essas chaves. O objetivo do atacante é
encontrar as chaves secretas
