# Anotações (não entra no trabalho)

É uma rede Feistel que utiliza um SPN(?) como função Feistel. 
A camada não-linear da função Feistel é baseada na implementação bitsliced da S-Box, e pode ser vida como um instância da estratégia LS introduzidapor Robin e Fantomas (https://www.cryptolux.org/index.php/Lightweight_Block_Ciphers#Fantomas.2FRobin) 

Os objetivos do designer são:
1) Implementação eficiente em CPUS de 8-bits
2) Sem uso de tabela e SRAM
3) Baixo custo de decriptação
4) Segurança provável como na estratégia de "wide trail"

O uso da chave é bem simples: pedaços de 32-bits da chave mestre são usados um após o outro. Quando o fim da chave é alcançado, os primeiros bits são usados novamente. Constantes de turn são adicionadas para prever ataques laterais.

A função de Feistel tem uma estrutura SPN que consiste de 4 camadas S-Box, 3 camadas lineares (parecidas com muito parecidas com https://www.cryptolux.org/index.php/Lightweight_Block_Ciphers#ITUbee) e 3 adições de chaves. O S-Box de 4bit foi escolhido pelo circuito muito simples que pode ser usado para computar assim como por boas características criptográficas. Ele foi citado previamente no trabalho de Ullrich et al (Markus Ullrich, Christophe De Canniere, Sebastiaan Indesteege, Özgül Küçük, Nicky Mouha, and Bart Preneel. Finding optimal bitsliced implementations of 4×4-bit s-boxes. In SKEW 2011 Symmetric Key Encryption Workshop, Copenhagen, Denmark, pages 16–17, 2011.). E também é usado por Misteryon (https://www.cryptolux.org/index.php/Lightweight_Block_Ciphers#Mysterion).

A camada linear é aplicada em 4 bytes separadamente e consiste de XOR de 3 diferentes rotações da entrada (de um modo semelhante as funções F0 e F1 de HiGHT: https://www.cryptolux.org/index.php/Lightweight_Block_Ciphers#HIGHT)

* Slides: https://wiki.crypto.rub.de/lightsec15/slides/RoadRunneR%20A%20Small%20And%20Fast%20Bitslice%20Block%20Cipher%20For%20Low%20Cost%208-bit%20Processors.pdf

## Abstract

Desenhar/desenvolver cifras de bloco com foco em CPUs de 8-bit é um problema desafiador. Existem muitas cifras leves recentes desenvolvidas para uma performace melhor em hardware. Por outro lado a maior parte das cifras leves e eficientes possuem uma falta de prova de segurança, ou tem uma margem de segurança baixa. PAra preencher essa carência nós apresentamos o RoadRunner que é uma eficiente cifra de bloco para software de 8-bit, e sua segurança é provada contra ataques diferenciais e lineares. RoadRunner tem o menor tamanho de código em um Atmel's ATtiny45, exceto pelo SPECK da NSA, que não tem prova de segurança. Além disso nós propomos uma nova métrica para comparação justa de cifras de bloco. O método chamado ST/A, é a primeira métrica que usa tamanho de chave como parâmetro para comparar chaves de tamanhos diferentes de uma maneira justa. Utilizando ST/A e outras métricas na literatura, nós provamos que o RoadRunner é competitivo entre outras cifras existentes no ATtiny45.

## Introdução

Com a baixa de preço de pequenos dispositivos eletrônicas tem se tornado mais popular noções de computação ubiqua, IoT e prédios inteligentes. Nessas aplicações é comumente utilizado RFID e cpus de 8-bits. ATtiny45 (um dos processadores de 8 bits  mais comumente usados) custa menos de 1 dólar. Essa disponbilidade torna essas CPU uma boa escolha para diversas aplicações como redes de sensores wireless.

Um dos principais problemas nessas aplicações é a segurança e privacidade da inforação trocada entre dispositivos. Em muitos casos os dados é compartilhados entre os nós e o servidor pelo ar. Com isso um atacante pode pegar informação privada, ou até muda-la em seu benefício. Por isso é um requsito utilizar algoritmos de criptografia  nessas aplicações. Cmo os dispositivos são limitados em termos de memória, frequência e energia, utilizar algoritmos criptográficos leves se torna a melhor opção para essas aplicações.

Nos últimos 10 anos cifras de bloco tem atraido a atenção. Muitas das soluções propostas nesses 10 anos utiliza building blocks para otimar implementações de hardware. Por esse motivo muitos desses algoritmos não são uma boa escolha para implementação de aplicação em CPUs de 8 bits. Por outro lado outros algoritmos são desenvolvidos com foco na performace de software para ser uma alternativa a CPUs de baixo custo. Algumas dessas cifras recentes utiliza (bitslice substitutions layers- S-boxes) onde operações booleanas nas palavras de CPU são utilizadas para descrever a S-box. Por essa abordagem tabelas look-up pode ser evitadas economizando tamanho do código e ciclos de clock de CPU. Além disso, como cifras bislice usam pequenos S-boxes, a suas áreas de hardware são pequenas.

Um outro problema no design de cifras de bloco é a comparação de eficiencia de difertens cifras para uma aplicação, e as vezes para fins acadêmicos. Cada plataforma e aplicação  tem suas próprias limitações (constraints) e uma comparação simples de area ou valores de throughput(taxa de transferência) não são suficientes nem justos. Formulas para comparar cifras de bloco utilizando características de area-speed (velocidade de area) são necessárias, desde que metódos de implementação afetam os 2 valores.

Throughput / ARea é uma métrica apresentada para tornar justa a comparação de cifras de bloco. (... continua falando de métodos de comparação, ver depois)



### Nossa contribuição

Nós desenhamos/desenvolvemos uma nova cifra de bloco leve, RoadRunner, que tem como objetivo eficiencia (especialmente em cpus de baixo custo de 8 bits) e provável segurança em termos de um número mínimo de S-boxes ativos abordagens diferenciais e lineares. A cifra é especialmente desenvolvida para ter um tamanho de código bem pequeno, enquanto tem um grande throughput. Simulações mostraram que no ATtiny45 nossa cifra tem o menor tamanho de código, exceto pelo SPECK (da NSA) que não tem propriedades de segurança provável para determinar um número de rodadas. Nossa análise preliminar mostrou que o RoadRunner tem uma relativa alta margem de segurança em contraste com a maioria das cifras leves. RoadRunner tem balanço de características de area-tempo-segurança com diferentes méotods de implementação, então ele pode ser adaptar as necessidades de uma aplicação específica que vai utiliza-lo.

Além disso, ... (método de comparação, ver depois)

##Definições e fundamentação do desenho do RoadRunner

No desenho/desenvolvimento do RoadRunner, os nossos objetivos principais foram os seguintes

* Implementação eficiente em CPUs 8-bit,
* Não usar tabela nem SRAM,
* Baixo overhead para desencriptar,
* Segurança provável assim como "wide trail design strategy" [17]

O foco principal foi memória reduzida. Isso devido ao fato de quê CPUs de 8 bit tem memória de programa de alguns KB. Na maioria das aplicações essa memória é compartilhada por alguns outros algoritmos (como rotinas de serviço de interrupção) e possivlemtne um SO de tempo real. Então reduzir a pegada(footprint) de memória é benéfico para nossa plataforma alvo. No artigo [39] é apresentado que a implementação de hardware de cifras de bloco leve com foco em RFID e WSNs deve custar menos que 2000 gate equivalent. PAra implementações de software não existe limite definido, mas nós acreditamos que 1 KB de memória não deve ser excedido para uma implementação de cifra de bloco leve.

### Estrutura geral

RoadRunner é uma cifra de bloco do tipo Feistel(mostrado na figura), como blocos de tamanho 64-bit e chaves de 80-bit ou 128-bit de tamanho. Chaves de 80-bit requerem 10 turnos e de 128-bits 12 turnos. Turnos inciais e finais de clareamento usam chavem de clareamento (Wk0 e Wk1) XOR's na parte esquerda do estado. Não existe operação de swap no round final. Decriptação usa a mesma função de turno onde a ordem das chaves de clareamento, chaves de round e constantes são invertidas.

(como o algoritmmo funciona):

Considerando a entrada como 8 bytes (64 bits) cada função de turno (F) pega os 4 bytes mais significantes (a esquerda: x0,x1,x2,x3) como entrada, uma constante Ci 1 byte e umachave de 96-bit(do turno atual). (f(ri,k[i],c[i]). O resultado é aplicado um xor com o restante da entrada (os outros 4 bytes - li, x4, x5, x6 e x7) .

F é uma função do tipo SPN 4 turnos (3 turnos de SLK, e 1 S-box). A função SLK é uma aplicação consecutiva de uma camada S-BOX(S), uma camada de difusão (L) e uma adição da chave (K).

Na função F, depois da aplicação da segunda SLK, a constante de turno (ci) é aplicada XOR com o byte menos significativo (mais a direita x3)

A constante Ci = NR - i, onde NR é o número de rounds, e i = 0,1,...., NR-1
Ci é representado como um inteiro 8-bit litle endian

O uso de constante de round previni ataques laterais simples, e faz com quê a função de round seja diferente para diferentes rounds. 

Nas camadas de S-Box, os bits da i-ésima posição é aplicado e uma outra posição (são 8 SBOx, cada um com 4 bits de entrada), e no resultado é colocado em uma nova posição.(isso é o melhor do S-Box em termos de segurança e eficiência - [42])

Depois da camada S-Box é aplicado uma matriz de difusão L a cada byte independentemente

### Camada S-Box

A tabela apresentada indica como o valor de entrada (de 4 bits - x0 a xF) se transformará no valor de saída(x0 a xF)


--> tentar entender o código assembly apresentado (não entendi a função do código assembly, não parece bater com o resultado

### Camada de difusão

Depois da camada S-Box é aplicada uma função linear em cada byte para prover difusão das palavras de 8-bit
É necessário uma função linear eficiente para operar bytes. Uma solução claássica é utilizar XOR de valores deslocados (shifted) e rotacionados.

No ATtyny45 (e na maioria de CPUs de baixo custo) não existe shift paramétrico e não tem instrução de rotação. Então para deslocar ou rotacionar muitos ciclos de clock são necessários o quê consme memória do programa  ciclcos de cloque. Uma rotação de 1-bit à esquerda pode ser feito com 2 instruções, utilizando a instrução ADC, enquanto uma rotação de 1-bit à esquerda pode ser feito com 3 ciclos usando as instruções BST e BLD. Existe ainda uma outra instrução que troca metade do byte, que resulta em uma rotação de 4-bit.

Usando essas instruções tentamos construir uma funçõa linear na forma de 
`L(x) = (x<<<i) ^ (x<<<j) ^ (x<<<k)` para usar no RoadRunner
onde `x<<<i` representa uma toação de i-bit da palavra da CPU x à esquerda.  Camadas lineares dessa forma são garantidamente inversíveis e todas tem ramo(branch) número 4. O número de ramo de uma matriz L é definido como à seguir:

BN(L) = min(x!=0) {hw(x) + hw(L(x))}

onde hw(x) denota o Hamming weight de um vetor binário x. Esse número dá o número mínimo de S-Boxes ativos em 2 turnos consecutivos. Além do número do ramo, nós cálculamos o número mínimo de S-boxes ativos em uma estrutura de SPN de 4 turnos de F com cada matriz L candidata. A tabela 1 mostra a melhor função linear (menos de 15 instruções para 2 multiplicações de matrizes)

Dessa tabela escolhemos L1 como a nossa matriz de difusão, desde que ela provê uma boa difusão e performace. O número mínimo de S-boxes ativos diferencialmente  em um F ativo usando as camadas lineares acima é calculado de uma maneira truncada, ou seja, é independente do S-box selecionaodo (???)

==>> fórmula ???


Matriz de difusão aplicada foi

`L(x) = x ^ (x<<<1) ^ (x<<<2)`

##### Código da camada de difusão

`; State registers
: X0,X1
; Temporary registers : T0,T1,ZERO (value in ZERO is 0)
movw T0,X0; T0,T1 <- X0,X1 (copia uma palavra de 16 bits)
;2 shifts - começo
lsl T0; msb of T0 is moved to carry flag -> faz 1 shift a esquerda ( bit "perdido" vai para C)
adc T0,ZERO ; Since ZERO is 0, this moves msb in carry to lsb (soma o bit perdido em T0)
eor T0,X0 
lsl T0
adc T0,ZERO
eor X0,T0
;2 shifts - fim
lsl T1
adc T1,ZERO
eor T1,X1
lsl T1
adc T1, ZERO
eor X1, T1`

### Escala de Chaves

RoadRunner pode ter chaves de 80 ou 128 bits. Para os ambos tamanhos de chaves, a escala de chave tem a mesma decrição: Inicia no começo de uma chave mestra, sempre que uma nova chave de 32-bit é requisitada continua na chave mestra de uma maneira circular.

Chaves de 128 bits serão dividias em 4 palavras de 32-bit: A||B||C||D . A whitening key inicial é A, a chave do primeiro round: B-C-D, do segundo round A-B-C., etc. Nesse caso, a cada 4 rounds, a mesma chave será usada

    Chaves de 80 bits serão divididas em 5 palavras de 16-bits: A||B||C||D||E , nessa organização a whitening key será A||B, a chave do primeiro turno será C||D-E||A-B||C, a chave do segundo turno: D||E-A||B-C||D, etc. Nesse caso o período para repetição da chave será 5 turnos, então o número de turnos tem que ser um múltiplo de 5.

## Segurança do RoadRunneR

Para ataques com número reduzido de turnos assumimos que existe whitening inicial e final nas versões reduzidas no lado esquerdo da cifra, e nenhuma troca/permutação é aplicada no útimo turno. O whitening final é a próxima palavra da chave na escala de chaves (?)

Whitening é aplicado somente no lado esquerdo para previnir ataques à partir de bits conhecidos de valores de turnos intermediários da cifra. Não é necessário aplicar XOR com a chave whitening nas metades direitas para prover essa porpriedade. Na maioria dos atques, extender o ataque para até 1 turno não é possível desde que isso necessita procurar por todas as chaves de 128-bits (na versão de 128bits) por causa do whitening. Então podemos dizer que as chaves de whitening desempenham uma regra de segurança crucial no RoadRunneR, e não pode ser omitido nos ataques.

Desde que a escola de chaves utiliza a chave mestra sem nenhuma modificação enquanto é utilizado nos turnos, nós não temos nenhuma pretensão (claim) contra ataques relativos à chave. Ataques relativos à chave são fáceis de serem defendidos em níveis de protocolo, e algumas das cifras leves não consideram também como PRINCE e PRIDE. Portanto utilizamos essa suposição no RoadRunner. De fato, cada F pode ser passado por 2 S-boxes ativos  em um ataque relativo à chave, com o total de 24 S-boxes ativos. Esse número total pode ser mais raduzido em uma análise mais detalhada.

### Ataques diferenciais e Lineraes

Ataques diferenciais e lineareas são os ataques mais bem sucedidos de cifras de bloco. RoadRunneR tem limites demonstráveis em um número mínimo de S-Boxes ativos diferenciáveis, o quê mostra que não existe características diferenciais úteis para 5 turnos ou mais. Desde a transposição da nossa camada de difusão tem propriedades similares e a função F tem uma estrutura simétrica, propriedades lineares e diferenciais (de uma maneira trucada) no RoadRunner são as mesmas. Etnão sempre que mencionarmos S-boxes ativos, queremos dizer ambos: sboxes ativos linearmente e diferencialmente.

Assim como mencionamos na seção 2.3 o número mínimo de Sboxes ativos em um F ativo é 10. Note que esse número é melhor que o valor sugerido pelo número de ramo (branch number), que é 8 para 4 turnos (desde que o número do ramo é 4). Existem 8 trilas truncadas de 10 e 11 Sboxes ativos, e todas começam com 1 Sbox ativo. Além disso, as caracteríticas com o mesmo padrão de entrada seguem o mesmo caminho para os turnos 2 e 3, somente o último turno é diferente para caminhos com mínimo peso. Esses caminhos são dados à seguir (ver imagem no artigo)

nós checamos experimentalmente algumas características diferenciais de alta probabilidade de F começando com 1 S-Box ativo, para ver se a propabilidade de característias e diferenciais são próximas ou não. Em nossos experimentos, nós não vímos nenhum aumento significativo na probabilidade diferencialda probabilidade característica teóricamente calculada. Então assumimos que cada S-box ativo multiplica a probabilidade com 2^(-2), e um F ativo tem probabilidade aproximada de F^(-20).

Nós também calculamos o número mínimo de S-boxes ativos em um RoadRunneR de r-turnos com 4<= r <= 6, novamente de uma maneira truncada. Isso foi feito por uma busca exaustiva, graças ao grafo mencionado na seção 2.3. Utilizanod este grafo nós podemos generalizar todas as possíveis transições de Sb-box ativas na função F, junto com o número mínimo de Sboxes ativas. Na nossa busca de um número mínimo de Sb-xoes ativas no RoadRunneR r-turnos, nós testamos todos possíveis diferentes padrões de entrada trunca para a cifra e seguimos um caminho de r-turnos usando uma ramificação sobre F e XORs Feistel, contando o número de S-boxes ativos em todas as funções F. Sempre que duas diferenças truncadas encontram um XOR do esquema Feistel, nós tentamos os 2 casos com diferença e sem diferença. A tabela 3 mostra o número mínimo de S-boxes ativos para turnos de 4 a 6, juntos com a porcentagem de S-boxes ativos.

(mostrar tabela)


Perceba que esses limites são melhores que os limites clássicos nas cifras de Feistel com função inversível F, que nos dá 2, 3 e 4 funções F ativas em 4, 5 e 6 palavras consecutivas respectivamente. Na abordagem clássica, desde que um F ativo tenha pelo menos 10 S-boxes ativos, o limite é 20, 30 e 40 Sboxes ativos para 4, 5 e 6 turnos, enquanto nós encontramos 26, 36 e 48 Sboxes ativos para esses números de turnos na nossa busca. Nós acreditamos que a porcentagem de S-boxes ativos são muito boas para uma camada linear leve. A tabela 3 mostra que não existe características diferenciais úteis (ou trilha linear) em 5 ou mais turnos do RoadRunneR, desde que a probabilidade é de pelo menos 2^(-72).

Nós listamos todos os caminhos com o número mínimo de Sboxes na nossa busca. Observando as trilhas, nós vimos que não existem agrupamentos nos melhores caminhos, ou seja, sem caminhos começando e terminando na mesma posição de Sbox ativa em 5 turnos. Isso dá confiança de que as probabilidades características e diferenciais são muito próxmas em toda cifra. Além disso, nós acreditamos que 5 turnos do RoadRunneR é seguro contra ataques clássicos diferenciais e lineares.

Existem muitos ataques derivados de ataques lineares e diferenciais. (cita alguns) . Esses ataques normalmente não apresetam resultados melhores que os originais. Desde que toda chave mestra é usada no primeiro e último turno com as chaves de clareamento, é difícil de aplicar ataques 1R e 2R.


### Ataque diferencial impossível

No ataque diferencial impossível, diferenciais truncadas com probabilidade 1 são usadas para encontrar uma contradição diferencial nos turnos do meio. ESsa contradição é então utilizada para eliminar chaves fortes nos turnos extras adicionados antes e depois da característica. Para cifras Feistel, existe uma característica diferencial impossível no 5o turno genérico,

Desde que a função de round do RR tem um estrutura de 4 rounds SPN, nós não podemos encontrar características diferenciais para mais de 5 turnos. Por outro lado, todo material de chave é utilizaod no primeiro e último turno, quando a chave de clareamento é considerada, então nós acreditamos não que o ataque diferencial impossível não pode ser alicado para mais de 6 turnos do RR.

### Ataque integral

O ataque integarl (ou ataque quadrado) é um ataque personalizado para a cifra de bloco SQUARE. É também aplicado ao Tijndael que se tornou AES, e muitas outras cifras. Neste ataque todos os possíveis valores são dados para blocos de bits específicos no texto claro, e outros bits são mantidos constantes. Após alguns turnos de encriptação uma soma expecífica (normalmente 0) em uma localidade de um bit do texto cifrado é esperado.

Devido aos 4 turnas de estrutura SPN na função F, dar todos os possíveis valores para um único Sbox não dá muitos turnos em um ataque integral. O melhor ataque pode ser alcançado com um bloco ativo de 32-bits no metade direita do texto plano, como segue:

(0, A) → (A, 0) → (A, A) → (B, A) → (?, B)

Então, A denota um bloco de 32-bits ativo onde todos os possíveis valores são vistos. B é um bloco balanceado, isso soma XOR dos valores são zero. Um bloco indeterminado é representado pelo símbolo ?. Essa características não pode ser extendida para mais trnos. Portanto, não não acreditamos que ataques quadrados possam ser aplicados por mais de 6 turons no RR

### Ataques do tipo MITM (Meet in the middle)

Todos os bits de estado são afetados por todos os bits de chave após 3 turnos do RR. Além disso quando as matching variable (variáveis correspondentes) do ataque MITM são selecionadas na metade direita do estado na saída do 3o turno, não é possível adicionar mesmo 2 turno devido ao fato que todas os bits da chave afetam aquela variável na direção da decriptação depois de 2 turnos. As mesmas ideias se aplicam para 2 turnos no começo e 3 turnos no fim devido à estrutura de Feistel. Consequentemente, o ataque MITM não pode ser aplicado para mais de 4 turnos.

Outros extensões do MITM normalmente utilizam características diferenciais truncadas com alta probabilidade sobre múltiplos turnos. No caso do RR, desde que a função F é um SPN de 4 turnos, nós acreditamos que esses ataques não são mais eficientes que o ataque MITM.

### Ataques de canal lateral

Cifras leves são vulneráveis à ataques de canais laterais. O atacante pode acessar dispositivos de baixo custo que tem a chave, e podem medir o tepmo de encriptação, dissipação de energia, radiação, etc. Portanto mecanisomos para protegere as cifras contra esses taques pode ser necessária em algumas aplicações.

Existe um trabalho uqe mostra que cifras com camadas não-lineares bitslice são mais fáceis de serem protegidas contra ataques de canal lateral como DPA (análise de energia diferencial). Como é o caso do RR (possuir esse tipo de camada), podemos dizer que o overhead causado por um mecanismo de proteção à DPA é baixo para nossa cifra.

## Uma nova métrica eficiente para cifra de bloco ST/A

Apresentamos uma nova métrica ST/A : Security times Throughput over Area (Segurança vez taxa transferência sobre Area). Nessa nova métrica nós inserimos o tamanho da chave na formula de métrica de eficiencia, visto que não existe um jeito justo de comparar cifras de blocos com chaves de diferentes tamanhos na literatura. Nós extendemos o T/A multiplicando pelo tamanho da chave (em bits).

Taxa de transferência é dado em bits-por-segudo e Area é gate equipvalent (GE) em uso de memória por hardware ou software.

Nós inserimos o tamanho da chave visto que o tamanho da chave aumenta a eficiência da cifra. Além disso outros parâmtros afetam a métrica de uma maneira multiplicativa. Então outras operações matemáticas 


## Análise de Performance

Nós simulamos o RR para processador ATtiny45 usando assembly AVR no Atmel Studio 6.2. As implementações são somente de encriptação, e o texto plano e a chave são lidos da SRAM, texto plano é encripatdo e escrito de volta ao mesmo lugar. Não existe uso de SRAM além da chave mestra e do texto plano, então somente o tamanho do código é dado como performace de area. Carregar a chave e o texto plano e guardar de volta o texto cifrado está incluído no temo de encriptação. Vários métodos de otimização foram aplicados. Na tabela 4, nós damos os resultaods de performance do bloco de cifra RR.

(tabela)

A versão 80bit de area otimizada tem um pouco mais de área que a versão 128-bits, isso ocorre devido ao gerenciamento de chaves ser mais complexo. A coluna de otimização mostra vários métodos aplicados. O método de otimização e área dá o menor tamanho de código que podemos alcançar. Isso é feito pelo extensivo uso de subrotinas que salva memória do programa. A otimização de velocidade, por outro lado, não usa subrotina para eliminar ciclos extras requisitados por chamadas à subrotinas. Métodos compactos são descritos á seguir:

* Compact1: É derivado da versão otimizada de area. Algumas subrotinas são removidas e códigos repetidos são escritos para ela. 
* Compact2: Derivado da versão otimizada de espaço. A parte de seleção de chave na versão otimizada de velocidade é trocada por uma subrotina.

Existem mais relações (trade-offs) com diferentes propriedades de tamanhos de código/ciclo de clock mas não incluímos elas no artigo. A partir disso eda tabela 4 nós vemos que o RR tem um bom balanço das propriedades de throughput/area/segurança. Se nós começar à partir da versão de velocidade otimizada, podemos reduzir a área para mais da metade e pagar uma penalidade de velocidade de menos da metade. Implementações mais rápidas ainda são relativamente pequenas, e implementações menores não são tão lentas.

A tabela 5 apresenta comparação do RR com outros algoritmos. Nós mostramos 4 valores de métricas de comparaao para cada cifra. As métricas são explicadas à seguir:
- T/A - taxa de transferência/Area (classico)
- T/A-FOAM  é a mesma métrica, com a definição de taxa de transferência no FOAM
-ST/A calculado multiplicando t/A pelo tamanho da chave
-ST/A-FOAM  mutilicado t/a-foam pelo tamanho da chave

Ao invés de utilizar Throughput/Area, escolhemos utilizar AreaXTempo (tempo para produzir 1 byte). Dessa forma menores valores são melhores. Para melhorar a compreensão normalizamos cada coluna de métrica de comparação. Para isso dividimos todos os números de uma coluna pelo menor valor na coluna.

No cálculo dos valores de FOAM procuramos pelo melhor ataque em cada cifra em termo do número de rounds, e usamos isso com o número de turnos para calcular o tempo de encriptação. ESse cálculo foi feito mutliplicando o clock original de encriptaao por `NR*/NR` , onde NR é o número do turno original e `NR*` é o número de rounds calculado pela ideia acima. Nós não excluímmos qualquer configuração inicial visto que não sabemos os detalhes de implementação de cada algoritmo. Nós também excluímos ataques relacionados a chave, visto que não temos reinvidicação de segurança para esse tipo de ataque. Para NOEKEON e SEA, nós usamos os limites encontrados pelos desenvolvedores devido a falta de criptoanálise na literatura destas cifras.

Na tabela (A) e (S) representa as versões otimizadas de area e velocidade, (c1) e (c2) compact 1 e compact2. 

A tabela mostra que a melhor cifra em termos da nossa métrica e de métricas clássica é a família SPECK. Mas essa família segue o princípio de desenho (ARX) e falta propriedades de segurança provável. Então o número de seleção do SPECK não tem racional científico. Além disso no ataque apresentado em [38] atuores reinvidicam que características diferenciais truncadas à serem encontradas no futuro podem extender o ataque de 26 rounds para mais rounds. 
Com isso s removermos SIMON e SPECK temos a seguinte foto das implementações restantes: 

RRR é o melhor algoritmo em termos de tamanho de código (exceto otimizações speed e C2) e margem de segurança. Quando a abordagem FOAM não é considerado, ou seja métricas T/A e ST/A, PRIDE supera todas as implementações outras implementações (RR vem logo atrás do PRIDE). Quando levamos em conta margem de segurança T/A-FOAM e ST/A-FOAM mostram que (A) e (c1) tem o melhor rank, PRID e outras implementações de RR seguem elas 
A taxa de transferência do RR não é a melhor em nenhuma implementação, a implementação mais rápida está na 3a posição de 8 cifras. 
Nós pensamos que o RRR é rápido suficiente para a maioria das aplicações de CPUS de 8-bits.  Valores negritos mostram os melhores valores na sua coluna (com excessão de SIMON e SPECK). Vários valores do RRR estão em bold se eles são melhores que todos os resultados anteriores


## TODO:

* colocar parte de comentários sobre a comparação com outros algoritmos

* acertar  referencias a imagens

* inserir manual do Attiny como referência da velocidade

* inserir código (se sobrar espaço)



Implementar decriptação

Decryption uses the same round function where the
order of whitening keys, round keys and constants are reversed.


# Dúvidas

* Feistel
* SPN
* building blocks (pq não é bom para cpus de 8 bits)
* Sbox  e bislice substitutions layers
* gate equivalent (GE)
* Sboxes ativos
* branch number
* ataques 1R e 2R
* ataque linear e ataque diferencial
* margem de segurança
* FOAM



