Implementation of RoadRunneR
https://www.cryptolux.org/index.php/Lightweight_Block_Ciphers#RoadRunneR

Original paper: http://eprint.iacr.org/2015/906.pdf

Trabalho da Disciplina de Algoritmos Criptográfico (MO442) - IC / Unicamp

http://www.ic.unicamp.br/~rdahab/cursos/mo422-mc938/2016-2s/Welcome_files/trabalho2_mo422.pdf

# How to use RoadRunneR

On prompt, on folder code:

`python RoadRunneR.py ks x k`

Where ks is key size (128), x is plaintext (64bits number) and k is the key (128bits number)
