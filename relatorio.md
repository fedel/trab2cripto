**Trabalho de Algoritmos Criptográficos - MO442**

_Aluno: Gabriel de Souza Fedel_

_Professor: Ricardo Dahab_ 

# RoadRunner: A Small and Fast Bitslice Block Cipher For Low Cost 8-Bit Processor
_RoadRunner: uma cifra de bloco bitslice pequena e rápida para processadores 8-bits de baixo custo_

Artigo que propõe a cifra disponível em  [@baysal2015roadrunner]

#Resumo do trabalho

Esse trabalho apresenta o RoadRunneR, algoritmo de cifra de bloco bitslice recente 
voltado para processadores de 8-bits de baixo custo. O RoadRunneR é implementado de 
forma que tenha um bom desempenho e utilize pouco espaço do processador, 
tornado-o viável para uso em aplicações como IoT, computação ubíquia e 
prédios inteligentes. Os resultados encontrados com o RoadRunneR mostra
que ele é competitivo entre outras cifras para o ATtiny45
(processador de 8-bits de baixo custo). Também explicamos o ST/A uma nova 
métrica de comparação de cifras de bloco apresentadas pelos mesmos 
desenvolvedores do RoadRunneR. Tal métrica é a primeira que leva em conta 
o tamanho da chave. O trabalho também compara a implementação original do 
RoadRunneR com uma implementação em Python desenvolvida pelo autor.

# Descrição do Algoritmo

## Autores

Adnan Baysal and Sühap Şahin

## Descrição em alto nível

O RoadRunneR é uma cifra de bloco bitslice para encriptação do tipo Feistel. O
RoadRunneR tem segurança demonstrável (em termos de S-boxes ativos em análises
diferencial e linear). O tamanho do bloco de entrada utilizado é 64 bits, e as 
chaves pode ser de 80 ou 128 bits. Ao usar chaves de 80 bits são usados 10 
turnos, enquanto chaves de 128 bits usam 12.

## Descrição dos detalhes do algoritmo

No desenvolvimento do RoadRunner foram considerado 4 principais objetivos:

* Implementação eficiente em CPUs de 8-bits
* Não utilizar tabela nem SRAM
* Baixo custo de desencriptação
* Provável segurança, assim como na "wide trail design strategy" (utilizada no desenvolvimento do AES)

Desses objetivos, um dos principais foi reduzir o uso de memória, para isso as
partes que compõe o RoadRunneR são implementadas de modo a utilizar o mínimo de
instruções.

![Estrutura geral do RoadRunneR\label{estRR}](./imageRR_1.png "Estrutura Geral do RoadRunneR")

![Função de Turno F\label{funcaof}](./imageRR_2.png "Função de turno F")

![Bloco SLK\label{slk}](./imageRR_3.png "Bloco SLK")

A figura \ref{estRR} mostra a estrutura geral do RoadRunneR, onde podemos 
conferir que ele possuí uma fase inicial e final de clareamento, feito com 
XOR's das chaves de clareamento (WK~0~ e 
WK~1~). O bloco de entrada é divido em duas partes, uma delas (mais à 
esquerda) é aplicada na função de turno (F) junto com a função de turno 
(Rk~i~) e uma constante de turno (C~i~), essa mesma parte alimenta então 
o próximo turno de forma invertida (indo para o lado direito). A outra parte 
da entrada mais à direita) é combinada com um XOR com a saída da função 
de turno F então é aplicada ao próximo turno também de forma invertida (indo para o
lado esquerdo). 

A função de turno, apresentada na figura \ref{funcaof},  é composta por 4 
turnos SPNs (Redes de substituição e permutação) . Os 3 primeiros são 
compostos por SLK : 1 camada S-box (S), 1 camada de difusão (L) e uma
adição de chave (K). Assim como mostrado na figura. Entre o segundo e o 
terceiro turno de F é aplicado a constante C~i~ aos bits mais à direita. 
O quarto turno de F é composto por uma camada S-Box.

A camada SLK, apresentada na figura \ref{slk}, é composta por uma camada de substituição feita com S-boxes, por uma camada de difusão (L) 
e por XORs com a chave. 

A seguir explicamos em detalhe as funções S-box, L (difusão), a função de
gerar chaves além de comentar sobre os aspectos de segurança da cifra.

### S-Box

A camada S-Box (que compõe o bloco SLK, usado na função F) recebe como entrada 
um bloco de 32 bits. Ela é composta por 4 S-box de 4 bits. A função de uma cada 
S-Box é obscurecer a relação entre a chave e o texto cifrado, para isso ela 
realiza uma substituição dos bits.

No RoadRunner a tabela de substituição utilizada é apresentada na figura
\ref{sbox}

![Tabela de substituição do S-Box\label{sbox}](./sboxtable.png "Tabela de substituição do S-Box")

Essa camada S-Box foi encontrada utilizando o trabalho de [@ullrich2011finding], que realizou uma busca exaustiva em todas as possíveis S-Boxes de 4 bits. No mesmo trabalho é apresentado que a menor implementação é construída com 9 instruções, característica atingida pelo RoadRunneR.

A função S-Box utilizada tem correlação máxima e probabilidades diferenciais de 2^-2^

No artigo é apresentado o código assembly para Ttiny45 utilizado para a 
S-box de 4-bits. Na implementação apresentada no trabalho foi realizada i
uma implementação similar (porém em python).

### Camada de Difusão

A função desta camada é gerar confusão, fazendo os bits mudarem de 
posição. No RoadRunneR cada bloco da camada de difusão recebe um bloco de 8
bits de entrada,e como a entrada é de 32 bits, são necessários 4 blocos.

As funções clássicas realizam XOR da entrada com variações que tenham os 
bits deslocados e rotacionados. Desta forma foi construído uma camada de 
difusão na forma:

$$L(x) = (x <<< i) \oplus (x <<< j) \oplus (x <<< k)$$

Na função '<<< z' indica rotacionar por z bits à esquerda.

Para determinar os valores i, j e k , foi utilizado a função de peso de Hamming,
que dá o número mínimo de S-boxes ativos em dois turnos consecutivos. 
Á partir dos resultados da variação de i, j e k, atentando para o peso de Hamming 
e o desempenho que custaria à CPU chegou-se aos valores i = 0, j = 1, k = 2,
gerando a função:

$$L(x) = x \oplus (x<<<1) \oplus (x<<<2)$$

CPUs de 8 bits não possuem funções de rotação 
nem shifts paramétricos. Desta forma é necessário mais de um ciclo para
implementa-las e por isso o cuidado em não realizar muitas 
rotações ou shifts.


### Geração de Chaves

O RoadRunneR utiliza uma série de chaves (2 para clareamento e 1 para cada função de turno), desta forma precisa gerar essas chaves à partir da chave de entrada. Para gerar as chaves a chave mestra é divida em palavras e estas são utilizados para compor cada nova chave solicitado. 

Para chaves de 128 bits, são 4 palavras de 32-bits (A|B|C|D). Quando a 
primeira chave de clareamento é solicitada é enviado A; para a primeira 
chave de turno B|C|D; para a segunda chave de turno A|B|C; e assim por diante.
Para chaves de 80 bits são 5 palavras de 12-bits (A|B|C|D|E). Quando a
primeira chave de clareamento é solicitada é enviado A|B; para a primeira 
chave de turno C|D|E|A|B; para a segunda chave de turno C|D|E|A|B; e assim por
diante. Desta forma pedaços da chave mestra são utilizados de maneira circular. 

Utilizando este método de geração de chaves ocorre que as chaves de turno se
repetem (a cada 5 turnos quando a chave de entrada for de 80 bits e a cada 4
turnos se for 128 bits). A tabela apresentada na figura \ref{chaves} mostra 
como são as chaves geradas
para o RoadRunneR nos 2 modos (80 e 128 bits):

![Esquema de geração de chaves do RoadRunneR\label{chaves}](./geracaochaves.png "Esquema de geração de chaves do rr")

### Aspectos de Segurança


O RoadRunneR tem limites demonstráveis que garantem não existência de
características diferenciais úteis para 5 turnos ou mais (aumentando sua
segurança contra ataques diferenciais).
As propriedades lineares e diferenciais no RoadRunneR são as mesmas, assim as
propriedades de segurança se refletem para questões lineares e diferenciais.

No artigo que apresenta o RoadRunneR, ele se mostra seguro à ataques clássicos lineares e diferenciais utilizando um número de turnos superior a 5.

Os ataques diferencial impossível e integral também não parecem ser efetivos
contra o RoadRunneR. O ataque MITM também não pode ser aplicado à essa cifra
para mais de 4 turnos. 

Ataques de canais laterais são efetivos contra cifras leves.
Mesmo com esse fato, como o RoadRunneR é um algoritmo tem camadas não-lineares
bitslice, o overhead para protege-lo do DPA (análise de energia diferencial, um
dos ataques de canal lateral) não é grande.


## Desempenho

No artigo que apresenta o RoadRunner é realizado uma análise de desempenho
da cifra em um processador ATtiny45. Os testes consideram somente a 
encriptação e utilizam a SRAM somente para ler o texto plano e a chave
e escrever o texto cifrado. 

### Análise de desempenho e espaço

A primeira análise realizada é em relação ao espaço que o código utiliza e 
quantos ciclos de processador ele utiliza. O resultado pode ser conferido na 
figura \ref{desempenho}.

![Comparação de desempenho do RoadRunner\label{desempenho}](./performance1.png "Comparação de desempenho do RoadRunner")

Os autores fizeram diversas implementações para conseguir ocupar pouco 
espaço e consumir menos ciclos do processador. Para as implementações com
menor consumo de espaço (otimização Area, na tabela) foi utilizado 
excessivamente subrotinas. Já para conseguir um melhor desempenho
(otimização Speed na tabela) foi retirado todas as subrotinas.
As otimizações Compact-1 e Compact-2 tem implementações intermediárias,
utilizando parcialmente subrotinas.

À partir dessas análises e de outras não apresentadas no artigos os 
autores concluem que o RoadRunneR tem um bom equilíbrio de 
taxa de transferência/área/segurança, conseguindo reduzir tamanho 
de código e velocidade de execução com pequenas modificações.

### Métrica ST/A

O artigo que apresenta o RoadRunneR também apresenta uma nova métrica eficiente para comparar cifras de bloco : a ST/A (Segurança vezes Taxa de transferência sobre Área). Tal métrica considera o tamanho da chave como parâmetro para o cálculo de eficiência. Desta maneira algoritmos com tamanhos de chave diferentes podem ser calculados de maneira mais justa, visto que o tamanho da chave aumenta a eficiência da cifra.

### Comparação do RoadRunneR com outras cifras

[performance]:./performance2.png "Comparação de desempenho do RoadRunner com outras
cifras"


O RoadRunneR também é comparado com outras cifras de blocos, na figura \ref{comparacao} . 

!["Comparação de desempenho do RoadRunner com outras
cifras"\label{comparacao}][performance]

A comparação é feita utilizando-se 4 métricas:

* T/A : métrica clássica Taxa de Transferência / Área
* T/A-FOAM : a mesma métrica com a definição de taxa de transferência em FOAM
* ST/A: Calculado multiplicando T/A pelo tamanho da chave
* ST/A-FOAM : Calculado multiplicando T/A-FOAM pelo tamanho da chave

Para o cálculo de T/A foi utilizado Área * Tempo (considerando o tempo para produzir 1 byte), dessa forma números menores são melhores.
Com intuito de facilitar a leitura dos dados, cada coluna foi normalizada dividindo todos os valores de uma coluna pelo menor valor da coluna.

Para o cálculo de FOAM utilizou-se o número de turnos do melhor ataque em
cada cifra (NR\*), e esse número foi usado para calcular o tempo de
encriptação. O cálculo foi feito multiplicando-se o clock por `NR*/NR`, 
onde NR é o número de turnos originais.

Descontando a família SPECK, que não possuí propriedades de segurança provável,
o resultado mostra o RoadRunneR (representado na tabela por RRR) como melhor
algoritmo em tamanho de código e nível de segurança. Desconsiderando a
abordagem FOAM, o PRIDE fica em primeiro lugar com o RoadRunneR logo em
seguida.

Para o quesito velocidade, o RoadRunneR não tem o melhor desempenho, porém no
melhor desempenho fica em terceiro colocado. Entende-se que ele é rápido o
suficiente para as aplicações em CPU de 8 bits.

As referências indicadas na tabela são as seguintes:

* [24]: [@engels2013non]
* [3]: [@garay2014advances]
* [28]: [@karakocc2013itubee]
* [5]: [@ray2013simon]

## Histórico do algoritmo

O RoadRunneR é uma cifra de bloco relativamente nova, apresentada pela 
primeira vez no Workshop Internacional de Criptografia Leve para Segurança
e Privacidade - [LigthSec2015](https://wiki.crypto.rub.de/lightsec15/accepted.html) 
em [@baysal2015roadrunner] .

Em [@yang2016extension] é explorado características diferenciais truncadas 
do RoadRunneR. Os autores deste trabalho encontram características diferenciais
em 5 turnos, permitindo um ataque no no RoadRunneR-128 de 7 turnos sem 
chave de clareamento. Esse ataque tem complexidade de dados de 2^55^ de texto claro 
escolhido, complexidade de 2^121^ de encriptação, e complexidade de memória 
de 2^68^. Esse é o primeiro e melhor ataque conhecido para o RoadRunneR.

Além dessas duas citações não encontrou-se outras referente à criptoanálise 
dessa cifra, provavelmente pelo pouco tempo de seu lançamento. Por outro lado
outros artigos de cifras leves citam a comparação apresentadas no artigo do 
RoadRunneR (apresentada neste trabalho na seção "Comparação do 
RoadRunneR com outras cifras")

# Implementações de referência

### Quais e onde estão

Somente no artigo original [@baysal2015roadrunner] é apresentado parte do
código, no caso a camada S-Box e a camada de difusão L. Os códigos 
são apresentados em assembly do processador ATtiny 45.

**_Implementação do S-Box_**

A implementação da camada S-box é apresentada à seguir. Nela X0, X1, X2 
e X3 são respectivamente os 4 bytes de entrada, e T0 uma palavra temporária.

```
; S-box layer
mov T0,X3 ; State words : X0,X1,X2,X3
and X3,X2 ; Temporary word : T0
eor X3,X1
or X1,X2
eor X1,X0
and X0,X3
eor X0,T0
and T0,X1
eor X2,T0
```

**_Implementação da camada de difusão L_**

À seguir apresentamos a implementação original da camada de difusão L. 
Nela X0 e X1 representam os bytes de entrada, e T0 e T1 palavras 
temporárias.

```
; State registers
: X0,X1
; Temporary registers : T0,T1,ZERO (value in ZERO is 0)
movw T0,X0
; T0,T1 <- X0,X1
lsl T0
; msb of T0 is moved to carry flag
adc T0,ZERO ; Since ZERO is 0, this moves msb in carry to lsb
eor T0,X0
lsl T0
adc T0,ZERO
eor X0,T0
lsl T1
adc T1,ZERO
eor T1,X1
lsl T1
adc T1,ZERO
eor X1,T1

```

### Comparação

Nessa seção comparamos a implementação apresentada neste trabalho, e a implementação
apresentada pelo artigo do RoadRunneR. De acordo com o artigo que apresenta o
RoadRunneR sabemos que o pior resultado de tempo de encriptação para uma 
chave de 128 bits é 3819 ciclos. Considerando que o Atiny 45 executa 1Mhz de
instruções por segundo, ou seja 1 * 10^6^, temos que o tempo de execução é de
aproximadamente 3,8 * 10^3^ segundos. Esse tempo considera somente a execução
do algoritmo sem contar o tempo para carregar o texto plano e a chave mestra.

A implementação apresentada neste trabalho teve um resultado aproximado de 3,5 *
10^4^, considerando os 3 melhores valores de 1000 execuções. Este teste foi
executado em uma máquina com processador x86 64 bits, com núcleo de 17000Mhz. 
Para realizar esse teste foi utilizado o módulo timeit do python, da segiunte
forma:

```python -m timeit -s 'from RoadRunneR import RR128' 'RR128(0,0)'```

A implementação desenvolvida para o trabalho não foi feita com intuito de 
desempenho, mas sim de ficar clara e compreensível. Desta forma o desempenho
apresentado certamente não é o melhor possível. O fato de ser utilizado uma 
linguagem de alto nível interpretada (Python) também influí no desempenho. 

Considerando que os processadores utilizados para os testes são diferentes, 
para uma comparação mais apurada deveria ser feito testes com o mesmo 
processador.
 

# Implementação

A implementação realizada para o trabalho foi realizada utilizando python 
(testado na versão 2.7 e 3.4) e está disponível em:

[https://gitlab.com/fedel/trab2cripto](https://gitlab.com/fedel/trab2cripto)

O código se encontra na pasta code, e dentro dela um arquivo README.md com as
instruções de uso.

# Licenças

Com excessão das imagens retiradas de [@baysal2015roadrunner], este relatório  está licenciado sob a
Licença [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
O código apresentado no trabalho está licenciado sob
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

# Referências
