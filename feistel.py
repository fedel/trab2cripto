
msize = 64



def f(value, key):
    """Round Function
    Receives value and key"""
    pass

def crypt(m, k, n):
    """Cript function
    Using m size 64 bits
    m: plain text
    k: keys (list of subkeys)
    n: rounds"""
    # divide plain text 
    # get r=r(i-1) and l=l(i-1)
    l = (m & 0xFFFFFFFF00000000) >> 32
    r = (m & 0xFFFFFFFF)

    for i in range(n):
        # calculate li and ri
        li = r
        ri = l ^ f(r,k[i])
        l = li
        r = ri

    return (ri, li)
def decrypt():
    pass
