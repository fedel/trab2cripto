from sys import argv 

def rol(x, rbits, maxbits):
    """Rotate number to left
    x: value
    rbits: bits to rotate
    maxbits: maximum bits"""
    return (x << rbits % maxbits) & (2**maxbits-1) | \
            ((x & (2**maxbits-1)) >> (maxbits-(rbits % maxbits)))

def L(x):
    """Difusion layer
    i = 0
    j = 1
    k = 2
    x: 8 bit number"""
    return  x ^ rol(x,1,8) ^ rol(x,2,8)

def S(x):
    """32 bit S-Box
    x: 32 bit number
    """
    # break x
    xb = [0,0,0,0]
    
    xb[0] = (x & 0xFF000000) >> 24
    xb[1] = (x & 0x00FF0000) >> 16
    xb[2] = (x & 0x0000FF00) >> 8
    xb[3] = (x & 0x000000FF)

    t0 = xb[3]
    # 2nd and 3nd lines of algorithm paper
    xb[3] = (xb[3] & xb[2]) ^ xb[1]
    # 4nd and 5nd lines of algorithm paper
    xb[1] = (xb[1] | xb[2]) ^ xb[0]
    # 6nd and 7nd lines of algorithm paper
    xb[0] = (xb[0] & xb[3]) ^ t0
    # 8nd and 9nd lines of algorithm paper
    xb[2] = (t0 & xb[1]) ^ xb[2]
    
    # return xb separeted and concatened
    return xb,(xb[0] << 24) + (xb[1] << 16) + (xb[2] << 8) + xb[3]

def SLK(x,kj):
    """SLK
    x:32 bit number
    kj: a 32-bit piece of ki round key"""
    # rs : result of S
    rs,_ = S(x)

    rl = []
    # result of l
    for i in range(4):
        rl.append(L(rs[i]))

    r = (rl[0] << 24) + (rl[1] << 16) + (rl[2] << 8) + rl[3]

    return (r ^ kj)

def F(x,ki,ci):
    """F function
    The ci constant is XORed to the least significant byte
    x: 32 bit number
    ki: 96 bit round key
    ci: 8 bit round constant
    """
    # result of rounds
    r =[0,0,0,0]

    kj = [0,0,0]
    # break ki 
    kj[0] = (ki & 0xFFFFFFFF0000000000000000) >> 64
    kj[1] = (ki & 0x00000000FFFFFFFF00000000) >> 32
    kj[2] = (ki & 0x0000000000000000FFFFFFFF)

    # rounds
    r[0] = SLK(x,kj[0])
    r[1] = SLK(r[0], kj[1]) ^ ci
    r[2] = SLK(r[1], kj[2])
    _,r[3] = S(r[2])

    return r[3]


def keySchedule128(k):
    """Key Schedule
    Fixed to 12 rounds
    k: 128 key
    returns: 
        wk: a 2 position vector with initial an final whiteinng key
        rk: a 12 positions vector with 96 bit round keys
    """
    # divides k in 4 pieces
    A = (k & 0xFFFFFFFF000000000000000000000000) >> 96
    B = (k & 0x00000000FFFFFFFF0000000000000000) >> 64
    C = (k & 0x0000000000000000FFFFFFFF00000000) >> 32
    D = (k & 0x000000000000000000000000FFFFFFFF)

    # withening keys
    wk = [A,B]

    # round keys
    rk = [0,0,0,0,0,0,0,0,0,0,0,0]
    rk[0] = rk[4] = rk[8] = (B << 64) + (C << 32) + D
    rk[1] = rk[5] = rk[9] = (A << 64) + (B << 32) + C
    rk[2] = rk[6] = rk[10] = (D << 64) + (A << 32) + B
    rk[3] = rk[7] = rk[11] = (C << 64) + (D << 32) + A

    return wk,rk
def RR128(x, k, n=12):
    """RoadRunneR
    x: 64 bit number (plain text)
    k: 128 bit key
    n: turns"""
    # initial break number
    # li: i-th left part of x
    # ri: i-th right part of x
    li = (x & 0xFFFFFFFF00000000) >> 32
    ri = (x & 0x00000000FFFFFFFF)

    # keys
    wk, rk = keySchedule128(k)

    # initial whitening
    li = li ^ wk[0]

    #rounds
    for i in range(n):
        ci = n - i 
        # r(i+1)
        li_1 = li
        #l(i+1)
        ri_1 = F(li, rk[i], ci) ^ ri
        ri = li_1
        li = ri_1

    # final whitening
    li_1 = li_1 ^ wk[1]

    # result is combination of li_1 and ri_1
    r = (li_1 << 32) + ri_1

    return r


if __name__ == "__main__":
    """Arguments
    0: key size
    1: message - plain text
    2: key
    
    example:
        RoadRunner.py 128 0 0
    """
    keysize = int(argv[1])
    m = int(argv[2])
    k = int(argv[3])

    if keysize == 128:
        print(RR128(m,k))
