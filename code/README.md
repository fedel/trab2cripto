# Running

## To use this implementation on command line:

python RoadRunneR.py ks m k

Where ks is key size (only 128), m is plain text (a 64 bits number), and k is
key (a 128 bits number). The program will return the ciphered text.
Some examples to use:

python RoadRunneR.py 128 0 0 

python RoadRunneR.py 128 0x0000000000000002 0x80000000000000000000000000000000 

python RoadRunneR.py 128 0xFEDCBA9876543210 0x0123456789ABCDEF0123456789ABCDEF

(this are example from original paper)

It is possible to use RoadRunneR inside a python code or python prompt, on de
this folder do:

import RoadRunner
RR128(m,k)

Where m is 64 bits plain number and k is 128 bits key
